Contributing to the Poezio-omemo project
========================================

To contribute, the preferred way is to commit your changes on some
publicly-available git repository (on a fork `on codeberg
<https://codeberg.org/poezio/poezio-omemo>`_ or on your own
repository) and to notify the developers with either:
 - a ticket `on the bug tracker <https://codeberg.org/poezio/poezio-omemo/issues/new>`_
 - a merge request `on codeberg <https://codeberg.org/poezio/poezio-omemo/pulls>`_
 - a message on `the channel <xmpp:poezio@muc.poez.io?join>`_
