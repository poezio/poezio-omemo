# Log

Log of eevvoor's omemo plugin installation via pip on debian sid (siduction).

In the pip environment:

```bash
(poeziopip) $ pip install protobuf==3.20.3
Collecting protobuf==3.20.3
  Downloading protobuf-3.20.3-py2.py3-none-any.whl (162 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 162.1/162.1 kB 3.2 MB/s eta 0:00:00
Installing collected packages: protobuf
  Attempting uninstall: protobuf
    Found existing installation: protobuf 4.22.1
    Uninstalling protobuf-4.22.1:
      Successfully uninstalled protobuf-4.22.1
Successfully installed protobuf-3.20.3
```

```bash
$ python3 
Python 3.11.2 (main, Mar  5 2023, 08:28:49) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import poezio_omemo
Using slower stringprep, consider compiling the faster cython/libidn one.
>>> import pkg_resources
>>> 'omemo' in map(lambda e: e.name, pkg_resources.iter_entry_points('poezio_plugins'))
True
>>> omemo.version.__version__
'0.14.0'
>>>
```
