#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2019 Maxime “pep” Buquet <pep@bouah.net>
#
# Distributed under terms of the GPLv3+ license.

"""
    Poezio OMEMO plugin
    Copyright © 2021 Maxime “pep” Buquet <pep@bouah.net>
    This file is part of poezio-omemo.

    See the file LICENSE for copying permission.
"""

__version__ = "0.7.0"
__version_info__ = (0, 7, 0)
